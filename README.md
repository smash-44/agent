# smash - Simple Monitoring and Alerting System for Home

Smash (ugh) is a basic monitoring and alerting system for home data centres
and, I suppose, really small offices.  The intended target is sites with a
need to monitor a handful of systems without investing the time or energy in a
more full-fledged system like Nagios or Zabbix or a more complex logging and
monitoring stack.  The seminal use case is my small rack of old computers
operating in my furnace room--nothing fancy, but definitely inconvenient when
filesystems fill up or a system's OS is way out of date and I don't know about
it.

## Overview

Smash has a client-server architecture where one or more clients periodically
query or test local components and register the results with the server
through a REST API.  The server records the results, displays them to the user
and issues alerts.

Each client runs configured checks via the Smash _agent_, a Python
application, typically through a cron job.

The server is a Flask app with an SQLite backend database and provides the
dashboard, administrative control, and the API server the agents use.  A
separate server process periodically queries the database for results older
than some threshold to check for an optionally alert on stale results, which
may indicate a server or service is offline.

Smash's objective is to fulfill its monitoring and alerting role while being
as simple as possible to set up and use.

## Status

The author currently uses Smash to monitor a half-dozen old computers in his
home data centre and some development/experimental services at work, which do
not yet merit integration to the central monitoring stack.

### Functionality

The Agent and Server components are both essentially functional and the Server
provides a user dashboard displaying last registered results for all nodes and
their tests.

Planned functionality:

* _Alerting engine_ to trigger alerts based on configurable logic on receiving
results from nodes.
* _Currency checking task_ to periodically check that recorded results are
sufficiently recent.
* _Administrative dashboard_ to configure the above and also provide node
management--such as deleting nodes registered with the wrong name by mistake,
whoops.

## Architecture and design

### Authentication and Authorization

Smash does not itself provide any sort of authentication or authorization.  In
a home data centre, for example, the internal network should be isolated such
that only family members (and perhaps some trusted visitors using the WiFi)
would be able to view status or use the administrative functions--or, I
suppose, register an agent and start submitting test results.

If necessary, the frontend webserver could be configured to provide any
necessary access control.  One possible solution would be to require
client-side certificates to use the user or admin dashboards.

The exception is that agents use a simple randomly generated key to
communicate with the server.  The key is randomly generated on agent
registration and used thereafter for identification to the server.

## Agent

The Agent runs on each host, checking status of things as configured and
reporting to the server.  A sample configuration is provided along with
multiple _checks_, tests to run, and sample configuration for those, where
applicable.

A limited Agent written to be compliant with POSIX Shell is provided for hosts
without a Python 3 interpreter.  It is not as flexible as the primary Agent,
and Python 3-based checks probably won't work, but for smaller or embedded
systems, it can help bridge the gap.

### Checks

A check can be any script or program in any language that produces a small
(one line or shorter) amount of text and an exit status indicating the
severity of the result.  Both are reported on the dashboard and in any
alerts generated, with the severity used in presentation and alerting logic as
appropriate.

The text result should be simple, clear, and short.

### Severity

* 0 - Okay - everything's fine
* 1 - Info - everything's fine but there may be actionable results.  For
  example, an OS update test might find there is an optional update or
  cleanup that could be performed.  If "Okay" is presented as green, "Info"
  could be presented as light green.
* 2 - Unknown - unknown condition due to script failure or some other error in
  determining status
* 3 - Warning - there is a problem condition you should be aware of and maybe
  do something about, but not necessarily right now
* 4 - Error - something is broken or about to become so, you should definitely
  fix this
* 5 - Unusable - something is broken to the point that the service can't be
  used in any way and might be impacting other systems or services

As with syslog priorities and other such measures, determining what condition
means what severity level depends on multiple factors such as the relative
importance of a service, the risk incurred by the service being in a
particular state, the ability of other systems to absorb some of that risk,
and so on.

One example is the *disk usage check*.  For a `/var` filesystem, where filling
up completely can impede system operation and crash services, a reasonable
mapping of fullness (percent) to severity might be:

* Under 60% - Okay
* At 60% - Warning - definitely start thinking about adding space, cleaning up
existing usage, and/or managing use of the space more effectively through log
rotation or truncation, and so on.
* At 80% - Error - nothing's actually broken yet but things are definitely
going to go wrong if you're not careful.  In this case the error state is not
from the operation of the filesystem itself but that nothing's been done about
the space yet.
* At 100% - Unusable.  Things are failing, failed, and impacting other
services.

On a multiuser system, `/home` may follow a similar pattern, since one user
filling up space to 100% can impact other users' use of the system.  On a
single-user system, perhaps the "Unusable" designation isn't used at all,
because you know you just filled the thing up.

### Agent configuration and operation

The configuration of the agent is defined in a file (by default, `agent.conf`)
and an example of a minimal configuration would be:

```
[agent]
nodename = home-webserver
server = http://smash:8000
```

Here the nodename and the Smash server's base URL are defined.  The nodename
uniquely identifies the system under monitoring.  The server is the Smash
server.  This configuration is sufficient to _register_ the agent with the
server.  This is done as follows:

```
$ agent -r
```

The agent will internally generate a random key and register that with the
server under the given node name.  The generated key is then written to the
configuration file for subsequent runs.

Checks are defined in additional sections in the agent configuration.  For
example:

```
... 

[check:disk-usage]
script = checks/disk-usage
```

This section tells the agent to run the given script and report the results to
the server under `disk-usage` for the given node.

Note that scripts can be anywhere on the system and may include arguments.
They are interpreted by the shell.  _You can get a little fancy here if you
want to._

### Scheduling agent runs

Typically the agent will be scheduled to run via cron or some equivalent.  On
my systems, a basic cron job is:

```
32 * * * * $HOME/smash-agent/agent -q
```

This runs the Agent every hour at 32 minutes past, without reporting the
results to the console (just to the server).  (Why 32?  I typically specify the
next minute when setting up the cron job at first, such that I can almost
immediately verify the cron job has worked and the node is properly reporting
its results.  Then I leave it at that as a way of randomly distributing agent
check-ins over the given period.)

If you have some checks that should be run more or less often than others,
they can be specified in a separate configuration file with the same `[agent]`
section, and called from a separate cron job, like so:

```
32 * * * * $HOME/smash-agent/agent -q
15,45 * * * * $HOME/smash-agent/agent -c more-frequent.conf -q
```

### Other agent runs

It may be useful to run the agent at other times, such as on return from
system suspend or sleep, or on boot or before shutdown.

## Server

Flask app that responds to API requests and submissions from agents and builds
a comprehensive view of site health.  Provides a web interface to current
status.

### Running server under Docker

To run the Smash server the first time, or to reinitialize the database, use
something like:

```
docker run --name smash -e SMASH_INIT_DB=true -v /tmp/smash:/home/smash/instance -p 8080:80 smash
```

For subsequent runs, leave out the `SMASH_INIT_DB` environment variable to
reuse existing data.

### Alert daemon

Process that runs alongside server (?) checking whether anything is alertable.

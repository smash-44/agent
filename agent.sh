#!/bin/sh
# shellcheck disable=SC2086,SC2046
#
# Minimal Smash agent for directly reporting test results, or for smaller
# systems, or when Python3 not available.
#
# Usage:
#   agent.sh --server=<uri> [--nodename=<host>] [-r|--register|--key=<key>]
#            <checkprog> ...
#        OR  --server=<uri> [--nodename=<host>] [-r|--register|--key=<key>]
#            --report <test> <rc> <description>
#
# Runs specified checks and reports their results to the given server.

config=$HOME/.smash/config

# ---------------------------------------------------------------------------
#                                                             functions
# ---------------------------------------------------------------------------

send_results()
{
  # send_results $uri $nodename $key "$test" $rc "$output"

  tmpfile=$(mktemp $(basename $0).XXXXXX)
  curl -s -i -X POST \
    --data-urlencode key="$3" \
    -d node="$2" \
    -d test="$4" \
    -d status="$5" \
    --date-urlencode message="$6" \
    $1 > $tmpfile
  if grep -q 'HTTP/.* 201 CREATED' $tmpfile
  then
    rm $tmpfile
  else
    echo "Could not register result.  Server response in $tmpfile."
    return 1
  fi
}

# ---------------------------------------------------------------------------
#                                                       regular programming
# ---------------------------------------------------------------------------


register=0
report=0
while [ -n "$1" ]
do
  case "$1" in
    --config=*)
      config="${1#*=}"
      ;;
    --server=*)
      server="${1#*=}"
      ;;
    --nodename=*)
      nodename="${1#*=}"
      ;;
    -r|--register)
      # stop processing anything else
      register=1
      ;;
    -k)
      shift
      key="$1"
      ;;
    --key=*)
      key="${1#*=}"
      ;;
    --report)
      report=1
      # consume rest of arguments immediately
      shift
      test=$1
      shift
      rc=$1
      shift
      text="$*"
      ;;
    *)
      tests="$tests $1"
      ;;
  esac
  shift
done

if [ -z "$nodename" ]
then
  hostname=$(hostname)
  nodename=${hostname%%.*}
fi

if [ $register -eq 1 ]
then
  uri="${server}/api/node"
  echo "Registering node '$nodename' to server at $uri"

  # generate key
  if command -v base64
  then
    key=$(dd if=/dev/urandom bs=1 count=32 2>/dev/null | base64)
  elif command -v openssl
  then
    key=$(dd if=/dev/urandom bs=1 count=32 2>/dev/null | openssl base64)
  else
    echo "Could not generate key, no base64 or openssl"
    exit 1
  fi

  # register to server
  tmpfile=$(mktemp $(basename $0).XXXXXX)
  curl -i -X POST \
    -d name=$nodename \
    -d key=$key \
    $uri > $tmpfile
  if grep -q 'HTTP/.* 201 CREATED' $tmpfile
  then
    # save configuration
    mkdir -p $(dirname $config)
    cat >$config <<EOF
# Auto-generated on node registration.  Add checks as desired.
server=$server
nodename=$nodename
key=$key
EOF
    echo "Key written to $config"
    rm $tmpfile
    exit 0
  else
    echo "Could not register node.  Server response in $tmpfile."
    exit 1
  fi
fi

# read in configuration
if [ -f $config ]
then
  # shellcheck disable=1090
  . $config
else
  echo "No configuration found: '$config'"
  exit 1
fi

uri="${server}/api/result"

errs=0
if [ $report -eq 1 ]
then
  # whatever
  send_results "$uri" "$nodename" "$key" "$test" "$rc" "$text" || errs=$((errs+1))
else
  basedir=$(dirname $0)/checks
  for test in $tests
  do
    # execute test
    output=$($basedir/$test)
    rc=$?
    echo "Test $test, rc=$rc: '$output'"

    # register results
    send_results "$uri" "$nodename" "$key" "$test" "$rc" "$output" || errs=$((errs+1))
  done
fi

if [ $errs -gt 0 ]
then
  echo "Errors occurred."
  exit 1
fi

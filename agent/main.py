# pylint:

import sys
import os
import socket
import argparse
import configparser
import subprocess
import base64
import requests
from . import errors

# ---------------------------------------------------------------------------
#                                                              MODULE VARS
# ---------------------------------------------------------------------------

verbose = False

# ---------------------------------------------------------------------------
#                                                              FUNCTIONS
# ---------------------------------------------------------------------------

def fatal(template, *args):
  print('FATAL: ' + template % args)
  sys.exit(1)

def error(template, *args):
  print('Error: ' + template % args)

def info(template, *args):
  print(template % args)

def debug(template, *args):
  if verbose:
    print('DEBUG: ' + template % args)

def run_check(script, timeout=None):
  debug("Running check (script: %s, timeout: %s)", script, timeout)
  cwd = os.path.dirname(os.path.realpath(__file__))
  try:
    if sys.version_info[:2] <= (3, 5):  # <= Python 3.5
      res = subprocess.run(script, cwd=cwd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE, check=True, shell=True, timeout=timeout)
      output = res.stdout.rstrip().decode()
    elif sys.version_info[:2] == (3, 6):  # == Python 3.6
      res = subprocess.run(script, cwd=cwd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE, check=True, shell=True, timeout=timeout,
        encoding='utf-8')
      output = res.stdout.rstrip()
    else:  # >= Python 3.7
      res = subprocess.run(script, cwd=cwd, capture_output=True, check=True,
        shell=True, timeout=timeout, text=True)
      output = res.stdout.rstrip()
  except subprocess.CalledProcessError as e:
    if e.returncode > 4:
      rc = 1
      msg = f"[rc={e.returncode:d}]"
      if len(e.output.rstrip()) > 0:
        msg += f" {e.output.rstrip()}"
    else:
      rc = e.returncode
      msg = e.output.rstrip()
    return rc, msg
  return 0, output.rstrip()

# ---------------------------------------------------------------------------
#                                                                      MAIN
# ---------------------------------------------------------------------------

def main():
  # pylint: disable=global-statement
  global verbose

  # use short name of script directory as log tag
  # TODO: not logging.  Remove this or implement logging
  #_tag = os.path.basename(os.path.dirname(__file__))

  # determine installation directory
  install_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), os.path.pardir))

  # determine default config file paths
  # TODO: Default path relative to installed agent is portable but gross
  # Default paths are:
  # - agent install directory
  # - system configuration directory (not portable)
  # - XDG-standard local configurtion directory
  default_configs = [
    os.path.join(install_dir, 'agent.conf'),
    '/etc/smash/agent.conf',
    os.path.expanduser("~/.config/smash/agent.conf")
  ]

  # get command-line options
  parser = argparse.ArgumentParser()
  parser.add_argument("-r", "--register",
                      help="Register new agent",
                      action='store_true')
  parser.add_argument("-c", "--config", type=str,
                      help="Configuration file",
                      default=default_configs)
  parser.add_argument("-v", "--verbose",
                      help="Debug output",
                      action='store_true')
  parser.add_argument("-q", "--quiet",
                      help="No output",
                      action='store_true')
  parser.add_argument("--local",
                      help="Run locally only; do not register results",
                      action='store_true')
  parser.add_argument("check", help="Check to run, as defined in configuration", nargs='*')
  args = parser.parse_args()

  # debug output
  verbose = args.verbose

  # initialize configuration
  config = configparser.ConfigParser(delimiters='=')
  res = config.read(args.config)
  if not res:
    fatal("Could not read configuration in %s", args.config)

  # minimal configuration is agent
  if 'agent' not in config:
    fatal("Could not read agent configuration in %s", args.config)

  # determine node name
  if 'nodename' in config['agent']:
    nodename = config['agent']['nodename']
  else:
    nodename = socket.gethostname().split('.', 1)[0]

    # write it out to configuration if we're registering the node
    config['agent']['nodename'] = nodename

  # if we are registering the node
  if args.register:
    # How I'm thinking this could work:
    # retrieve API server's public key
    # generate secret key automatically
    # encode secret key with public key and include with node registration
    #
    # How it will work for now: generate key and send to server on registration

    # check if key is already defined
    if 'key' in config['agent']:
      fatal("Key already defined in config; node may already be registered.  Remove 'key' from config if not.")

    # generate secret key
    key = base64.b64encode(os.urandom(32)).decode('utf-8')
    config['agent']['key'] = key

    # build URL for node registration
    url = config['agent']['server'] + '/api/node'

    # build registration data
    registration = {
      'key': key,
      'name': nodename
    }

    # register with server
    try:
      # TODO: configurable timeout or at least a constant somewhere else
      r = requests.post(url, data=registration, timeout=5)
      if r.status_code != 201:
        fatal("Node registration failed with %s (%d): %s.", r.reason, r.status_code, r.text)
    except requests.exceptions.ConnectionError as e:
      debug("Connection error on URL %s: '%s'", url, e)
      fatal("Unable to connect to URL %s", url)

    # update configuration on disk
    with open(args.config, "w", encoding='utf-8') as config_fp:
      config.write(config_fp)

    info("Node key written to configuration file.")

    sys.exit(0)

  if not args.local:
    if not 'key' in config['agent']:
      fatal("No key configured.  Is this node registered?")
    key = config['agent']['key']

  # build list of checks to run, either from those specified on command line,
  # otherwise all
  if args.check:
    checks_to_run = [
      (name[6:], proxy) for (name, proxy) in config.items()
      if name.startswith('check:') and name[6:] in args.check
    ]
    if len(args.check) != len(checks_to_run):
      fatal("Not all requested checks found in configuration")
  else:
    checks_to_run = [
      (name[6:], proxy) for (name, proxy) in config.items()
      if name.startswith('check:')
    ]

  # parse and run checks
  for (test, defn) in checks_to_run:
    # script name defaults to test name if not specified
    script = defn.get('script', test)

    (rc, msg) = run_check(script, timeout=defn.get('timeout'))
    result_type = {
      errors.ERR_OK: 'Okay',
      errors.ERR_INFO: 'Info',
      errors.ERR_UNKNOWN: 'Unknown',
      errors.ERR_WARNING: 'Warning',
      errors.ERR_ERROR: 'Error',
      errors.ERR_UNUSABLE: 'Unusable'
    }[rc]

    if not args.quiet:
      print(f"{result_type}: {test}: '{msg}'")

    # build URL for recording test result
    url = config['agent']['server'] + '/api/results/'

    if not args.local:
      # build result record
      record = {
        'key': key,
        'node': nodename,
        'test': test,
        'status': rc,
        'message': msg
      }
      debug("Sending results record: %s", record)

      # register result with server
      try:
        r = requests.post(url, data=record)
        if r.status_code != 201:
          error("Failed to register test result for '%s' (%d, %s)", test, r.status_code, r.reason)
          debug("Further information from server: '%s'", r.text)
      except requests.exceptions.ConnectionError as e:
        debug("Connection error on URL %s: '%s'", url, e)
        fatal("Unable to connect to URL %s", url)

if __name__ == '__main__':
  sys.exit(main())

#!/bin/sh

_rcmax=0

higher()
{
  if [ $1 -gt $2 ]
  then
    _rcmax=$1
  else
    _rcmax=$2
  fi
  return $_rcmax
}

unusable()
{
  echo "$1"
  higher 5 $_rcmax
}

error()
{
  echo "$1"
  higher 4 $_rcmax
}

warning()
{
  echo "$1"
  higher 3 $_rcmax
}

unknown()
{
  echo "$1"
  higher 2 $_rcmax
}

info()
{
  echo "$1"
  higher 1 $_rcmax
}

okay()
{
  echo "$1"
  higher 0 $_rcmax
}

highest()
{
  return $_rcmax
}
